package com.compie.course.business.business.repository;

import com.compie.course.business.business.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {
    List<Item> findAllByName(String name);

    Item findById(long id);

    Item findItemByName(String name);
}
