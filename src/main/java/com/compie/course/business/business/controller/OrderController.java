package com.compie.course.business.business.controller;

import com.compie.course.business.business.entity.Client;
import com.compie.course.business.business.entity.Item;
import com.compie.course.business.business.entity.Order;
import com.compie.course.business.business.service.*;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/order")
public class OrderController {

    private final OrderService orderService;
    private final ClientService clientService;
    private final ItemService itemService;
    private final AssemblyLineManagerService assemblyLineManagerService;

    @PostMapping("/add")
    public @ResponseBody
    Order addOrder(@RequestBody Order order) throws URISyntaxException {
        clientService.saveClient(order.getClient());
        Order savedOrder = orderService.saveOrder(order);
        assemblyLineManagerService.addOrder(savedOrder);
        return savedOrder;
    }

    /**
     * 1. Client - passes the URI: localhost:8080/order/item?list=[...items]
     * 2. Server - create new 'Client' with name: null , _id: unique for him.
     * 3. Create new Order, set to the order the List<Item> - (1), set the Client (2)
     * <p>
     * Example: localhost:8080/order/item?list=meat,bread
     *
     * @param list
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/item")
    public @ResponseBody
    Order addOrderWithItemList(@RequestParam List<String> list) throws URISyntaxException {
        List<Item> itemList = itemService.getAllItemsFromList(list);
        Order order = orderService.createOrderWithItemList(clientService.saveClient(new Client()), itemList);
        assemblyLineManagerService.addOrder(order);
        return order;
    }

    @PatchMapping("/{id}")
    public @ResponseBody
    Order cancelOrder(@PathVariable(value = "id") Long id) {
        Order order = orderService.getOrderById(id);
        Order canceledOrder = assemblyLineManagerService.cancelOrder(order);
        if (canceledOrder != null) {
            order = orderService.saveOrder(canceledOrder);
        }
        return order;
    }

    @GetMapping("")
    public @ResponseBody
    List<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Order getOrderById(@PathVariable(value = "id") Long id) {
        return orderService.getOrderById(id);
    }

    @GetMapping("/client/{id}")
    public Order getOrderByClientId(@PathVariable(value = "id") Long id) {
        return orderService.getOrderByClientId(id);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody
    void removeOrderById(@PathVariable(value = "id") Long id) {
        orderService.removeOrderById(id);
    }

    @DeleteMapping("")
    public @ResponseBody
    void removeAllOrder() {
        orderService.removeAllInBatch();
    }

}
