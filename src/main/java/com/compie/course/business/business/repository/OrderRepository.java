package com.compie.course.business.business.repository;

import com.compie.course.business.business.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Order findByClient_Id(Long id);
    Order deleteByClient_Id(Long id);
}
