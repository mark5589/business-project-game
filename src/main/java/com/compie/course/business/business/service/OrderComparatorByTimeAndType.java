package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Order;

import java.util.Comparator;

/**
 * Comparator class for PriorityQueue.
 * keep the orders in the following priority: First sort by time of the order creation,
 * then by the Order type [Regular or Celeb] Celeb order pushed to the front of the queue.
 */
public class OrderComparatorByTimeAndType implements Comparator<Order> {
    @Override
    public int compare(Order order, Order t1) {
        return (order.getCreatedAt().compareTo(t1.getCreatedAt()) > 0 ?
                order.getType().compareTo(t1.getType()): 1);
    }
}
