package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Order;

import java.util.PriorityQueue;
import java.util.concurrent.CompletableFuture;

public interface AssemblyLine {

    CompletableFuture<PriorityQueue<Order>> processOrder(PriorityQueue<Order> ordersQueue) throws Exception;
}
