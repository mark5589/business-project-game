package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Order;
import com.compie.course.business.business.entity.Status;
import com.compie.course.business.business.exception.OrderNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * AssemblyLineManagerServiceImp is responsible for holding Orders which Status.IN_PROCESS in separate
 * PriorityQueue.
 * run Scheduled method, that check's if there is any Order in the Queue and if it is, then
 * he would allocate a new Thread that will process and assemble those queues.
 * The number of threads are decided by the number of processes that the machine has.
 * Another scheduled method, use another PriorityQueue which holds all Orders that had been assembled and
 * returned from the threads.
 * NOTE: the multithreading doesnt stop the main thread. in other words, the server keep working separately
 * and it's not affect the client experience in a bad manner.
 */
@Service
@Configuration
public class AssemblyLineManagerServiceImp implements AssemblyLineManagerService {

    private static final Logger logger = LoggerFactory.getLogger(AssemblyLineManagerServiceImp.class);
    private final AssemblyLine assemblyLine;
    private final OrderService orderService;

    public AssemblyLineManagerServiceImp(AssemblyLine assemblyLine, OrderService orderService) {
        this.assemblyLine = assemblyLine;
        this.orderService = orderService;
    }

    /**
     * Two Priority Queues ordersQueue - Just arrived orders. completedQueue - orders that had been processed and "assembled".
     */
    private PriorityQueue<Order> ordersQueue = new PriorityQueue<>(QUEUE_SIZE, new OrderComparatorByTimeAndType());
    private PriorityQueue<Order> completedQueue = new PriorityQueue<>(QUEUE_SIZE, new OrderComparatorByTimeAndType());

    private static final int CORE = Runtime.getRuntime().availableProcessors() - 1;
    private static final int QUEUE_SIZE = 100;
    public static final int CORE_QUEUE_SIZE = 10;
    private static final int SCHEDULED_RATE_SECONDS = 25000;

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(CORE);
        executor.setMaxPoolSize(CORE);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("AssemblyThread-");
        executor.initialize();
        return executor;
    }

    public void addOrder(Order o) {
        this.ordersQueue.add(o);
        logger.info("Just added, Main QUEUE:: " + this.ordersQueue.toString());
    }

    @Override
    public Order cancelOrder(Order order) throws OrderNotFoundException {
        try {
            for (Order o : this.ordersQueue) {
                if (o.getId().equals(order.getId())) {
                    o.setStatus(Status.CANCELLED);
                    return o;
                }
            }
        } catch (Exception e) {
            throw new OrderNotFoundException(order.getId());
        }
        return null;
    }

    /**
     * return subgroup from the orderQueue. the size of the returned queue is decided by the total amount
     * of the Orders in the queue divided by the number of available processors in the system.
     * if the size of orderQueue is less than the number of the processors, than it would use the whole group.
     */
    private PriorityQueue<Order> getQueueSubgroupForProcessing() {
        PriorityQueue<Order> coreQueue = new PriorityQueue<>(CORE_QUEUE_SIZE, new OrderComparatorByTimeAndType());

        if (!this.ordersQueue.isEmpty()) {
            int queueBound = this.ordersQueue.size() > CORE ? this.ordersQueue.size() / CORE : this.ordersQueue.size();
            logger.info("size of core:: " + CORE + ", size of bound::" + queueBound + " SIZE OF MAIN QUEUE:: " + this.ordersQueue.size());
            for (int i = 0; i < queueBound; i++) {
                coreQueue.add(this.ordersQueue.poll());
            }
        }
        return coreQueue;
    }

    private void pushAllElements(PriorityQueue<Order> fromQueue, PriorityQueue<Order> toQueue) {
        for (Order o : fromQueue) {
            toQueue.add(o);
        }
    }

    /**
     * Accesses the ordersQueue Object, get sub part of the queue, opens new thread that will
     * deal with the assembling of the order by using the AssemblyLine @Service, each order suspended to some amount of time,
     * using Thread.sleep(), at the end returns new PriorityQueue<Order> that pushed to completeQueue
     * Object.
     *
     * @throws Exception due the Thread.sleep(<The time that required to assemble a order>)
     */
    @Scheduled(fixedRate = SCHEDULED_RATE_SECONDS)
    public void processOrdersScheduled() throws Exception {
        logger.info("Starting scheduled task");
        List<CompletableFuture<PriorityQueue<Order>>> resultList = new LinkedList<>();
        for (int i = 0; i <= CORE && !isOrderQueueEmpty(); i++) {
            resultList.add(assemblyLine.processOrder(getQueueSubgroupForProcessing()));
        }

        for (CompletableFuture<PriorityQueue<Order>> futureRes : resultList) {
            pushAllElements(futureRes.get(), this.completedQueue);
        }

    }

    @Scheduled(fixedRate = SCHEDULED_RATE_SECONDS)
    public void saveOrdersScheduled() {
        for (int i = 0; i < this.completedQueue.size(); i++) {
            orderService.saveOrder(completedQueue.poll());
        }
    }

    private boolean isOrderQueueEmpty() {
        return this.ordersQueue.isEmpty();
    }
}

