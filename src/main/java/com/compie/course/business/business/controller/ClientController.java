package com.compie.course.business.business.controller;

import com.compie.course.business.business.entity.Client;
import com.compie.course.business.business.service.ClientService;
import com.compie.course.business.business.service.ResponseConverterService;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@ExposesResourceFor(Client.class)
@RequestMapping("/client")
public class ClientController {

    private final ClientService clientService;
    private final ResponseConverterService responseConverter;

    public ClientController(ClientService clientService, ResponseConverterService responseConverter) {
        this.clientService = clientService;
        this.responseConverter = responseConverter;
    }

    @PostMapping("/add")
    public @ResponseBody
    ResponseEntity<EntityModel<Client>> addNewClient(@RequestBody Client client) throws URISyntaxException {
        return responseConverter.getClientModelResponseOnPOST(clientService.saveClient(client));
    }

    @GetMapping("/all")
    public @ResponseBody
    ResponseEntity<List<EntityModel<Client>>> getAllClients() {
        return responseConverter.getClientListResponse(clientService.findAllClients());
    }

    @GetMapping("/{id}")
    public @ResponseBody
    ResponseEntity<EntityModel<Client>> getClientById(@PathVariable(value = "id") Long id) {
        return responseConverter.getClientModelResponse(clientService.getClientById(id));
    }

    @DeleteMapping("/{id}")
    public @ResponseBody
    ResponseEntity<EntityModel<Client>> removeClientById(@PathVariable(value = "id") Long id) {
        return responseConverter.getClientModelResponse(clientService.removeById(id));
    }

}
