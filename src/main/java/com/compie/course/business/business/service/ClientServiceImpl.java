package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Client;
import com.compie.course.business.business.exception.ClientNotFoundException;
import com.compie.course.business.business.repository.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This Service is a wrapper for the Object that implements JPARepository<> (ClientRepository)
 * as know Spring would implement this interface.
 */

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    // throws Client not found exception (404) if the client not found, that was created in ClientNotFoundAdvice
    public Client getClientById(Long id) {
        Client client = clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(id));

        return client;
    }

    public List<Client> findAllClients() {
        List<Client> clients = clientRepository.findAll()
                .stream()
                .collect(Collectors.toList());

        return clients;
    }

    public Client saveClient(Client client) {
        return clientRepository.save(client);
    }

    public void removeAllInBatch() {
        clientRepository.deleteAllInBatch();
    }

    @Override
    public Client removeById(Long id) {
        Client client = clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(id));
        clientRepository.deleteById(id);
        return client;
    }
}
