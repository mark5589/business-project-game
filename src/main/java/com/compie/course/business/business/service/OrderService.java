package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Client;
import com.compie.course.business.business.entity.Item;
import com.compie.course.business.business.entity.Order;

import java.util.List;

public interface OrderService {
     Order saveOrder(Order order);

     Order updateOrderById(Long id, Order order);

     Order getOrderByClientId(Long id);

     Order deleteOrderByClientId(Long id);

     Order createOrderWithItemList(Client client, List<Item> list);

     Order getOrderById(Long id);

     List<Order> getAllOrders();

     void removeAllInBatch();

     void removeOrderById(Long id);
}
