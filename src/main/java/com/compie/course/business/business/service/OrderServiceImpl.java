package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.*;
import com.compie.course.business.business.exception.OrderNotFoundException;
import com.compie.course.business.business.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Order saveOrder(Order order) {
        return orderRepository.save(order);
    }

    public void removeAll() {
        orderRepository.deleteAll();
    }

    @Override
    public Order updateOrderById(Long id, Order newOrder) {
        Order res = orderRepository.findById(id).get();
        orderRepository.findById(id).ifPresent(o -> {
            o.setItemsList(newOrder.getItemsList());
            o.setClient(newOrder.getClient());
            o.setStatus(newOrder.getStatus());
        });
        return res;
    }

    @Override
    public Order getOrderByClientId(Long id) {
        return orderRepository.findByClient_Id(id);
    }

    @Override
    public Order deleteOrderByClientId(Long id) {
        return orderRepository.deleteByClient_Id(id);
    }

    public void removeAllInBatch() {
        orderRepository.deleteAllInBatch();
    }

    @Override
    public Order getOrderById(Long id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(id));
        return order;
    }

    @Override
    public void removeOrderById(Long id) {
        orderRepository.deleteById(id);
    }

    @Override
    public Order createOrderWithItemList(Client client, List<Item> list) {
        Order order = new Order();
        order.setItemsList(list);
        order.setClient(client);
        order.setStatus(Status.IN_PROGRESS);
        order.setType(OrderType.REGULAR);
        return orderRepository.save(order);
    }
}
