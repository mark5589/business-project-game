package com.compie.course.business.business.resource;


import com.compie.course.business.business.controller.ClientController;
import com.compie.course.business.business.entity.Client;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Component
public class ClientResourceAssembler implements RepresentationModelAssembler<Client, EntityModel<Client>> {

    /**
     * @param client
     * @return EntityModel REST Form with link to the client uri and a link to all clients (GetMapping method),
     * in the future there should be more useful links for the user, ie. "cancel the order"
     */
    @Override
    public EntityModel<Client> toModel(Client client) {
        return new EntityModel<>(client,
                linkTo(methodOn(ClientController.class).getClientById(client.getId())).withSelfRel(),
                linkTo(methodOn(ClientController.class).getAllClients()).withRel("Clients"));
    }

    public CollectionModel<EntityModel<Client>> toModel(List<EntityModel<Client>> clients){
        return new CollectionModel<>(clients,
                linkTo(methodOn(ClientController.class).getAllClients()).withSelfRel());
    }
}
