package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Order;

public interface AssemblyLineManagerService {

    void addOrder(Order o);

    Order cancelOrder(Order order);
}
