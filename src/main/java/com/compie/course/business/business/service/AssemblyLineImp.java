package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Item;
import com.compie.course.business.business.entity.Order;
import com.compie.course.business.business.entity.Status;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.PriorityQueue;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AssemblyLineImp implements AssemblyLine {

    /**
     * The process that complete the assembling of the Order.
     * to make this process more concurrent, each new Thread will have his own set of
     * PriorityQueue, to process it separately.
     */
    @Async("taskExecutor")
    public CompletableFuture<PriorityQueue<Order>> processOrder(PriorityQueue<Order> ordersQueue) {
        return CompletableFuture.completedFuture(
                ordersQueue.stream()
                .map(order -> {
                    try {
                        Thread.sleep(getDelay(order) * 1000);
                        if (order.getStatus() == Status.IN_PROGRESS) order.setStatus(Status.COMPLETED);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return order;
                }).collect(Collectors.toCollection(() ->
                        new PriorityQueue<>(AssemblyLineManagerServiceImp.CORE_QUEUE_SIZE, new OrderComparatorByTimeAndType()))));
    }

    /**
     * Some random delay that were chosen by the sum of the order amount module 5.
     */
    private int getDelay(Order order) {
        double totalPrice = 0;
        for (Item item : order.getItemsList()) {
            totalPrice += item.getPrice();
        }
        return ((int) totalPrice % 5);
    }
}
