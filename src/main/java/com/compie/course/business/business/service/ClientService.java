package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Client;

import java.net.URISyntaxException;
import java.util.List;

public interface ClientService {

     Client getClientById(Long id);

     Client saveClient(Client client) throws URISyntaxException;

     Client removeById(Long id);

     List<Client> findAllClients();

     void removeAllInBatch();

}