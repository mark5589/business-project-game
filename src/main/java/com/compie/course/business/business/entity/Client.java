package com.compie.course.business.business.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Client class that describes the person who make the Order.
 * isCeleb = boolean, if it's a celebrity than later on the Queue the client would
 * get priority of '2' (the highest priority of all, priority reminder: 0 -   )
 */
@Entity
@Data
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    private Long id;
    private String name;
    private boolean isCeleb;

    @CreationTimestamp
    private LocalDateTime arrived;

    public Client() {}

    public Client(String name, Boolean isCeleb) {
        this.name = name;
        this.isCeleb = isCeleb;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isCeleb=" + isCeleb +
                ", orders=" + "temp_empty due to debug" +
                ", items=" +
                '}';
    }
}

