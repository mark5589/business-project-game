package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Item;
import com.compie.course.business.business.exception.ItemExistException;
import com.compie.course.business.business.repository.ItemRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    public List<Item> getAllItems() {
        return itemRepository.findAll();
    }

    public Item saveItem(Item item) {
        if(item.getId() == null) return itemRepository.save(item);
        itemRepository.findById(item.getId())
                .orElseThrow(() -> new ItemExistException(item));

        return itemRepository.save(item);
    }

    public void removeAll() {
        itemRepository.deleteAll();
    }

    @Override
    public Item getItemByName(String name) {
        return itemRepository.findItemByName(name);
    }

    @Override
    public void removeItemById(Long id) {
        itemRepository.deleteById(id);
    }

    @Override
    public List<Item> getAllItemsFromList(List<String> list) {
        List<Item> itemList = new LinkedList<>();
        for (String name : list) {
            itemList.add(itemRepository.findItemByName(name));
        }
        return itemList;
    }
}


