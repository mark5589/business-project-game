package com.compie.course.business.business.entity;

public enum Status {
    IN_PROGRESS,
    COMPLETED,
    CANCELLED;
}
