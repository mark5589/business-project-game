package com.compie.course.business.business.controller;

import com.compie.course.business.business.entity.Item;
import com.compie.course.business.business.service.ItemService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/item")
public class ItemController {
    private ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("")
    public @ResponseBody
    List<Item> getAllItems() {
        return itemService.getAllItems();
    }

    /**
     * Some random prices that generated for the items (in our case burger's ingredient)
     *
     * @return
     */
    @PostMapping("/populate")
    public @ResponseBody
    String populateItems() {
        Random r = new Random();
        double rangeMin = 10.0;
        double rangeMax = 25.0;
        String[] itemNames = {"bread", "meat", "onion", "tomato"};
        for (String s : itemNames) {
            double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
            Item item = itemService.saveItem(new Item());
            item.setName(s);
            item.setPrice(randomValue);
            itemService.saveItem(item);
        }

        return "Item DB populate";
    }

    @PostMapping("/add")
    public @ResponseBody
    Item addItem(@RequestBody Item item) {
        return itemService.saveItem(item);
    }

    @GetMapping("/{name}")
    public @ResponseBody
    Item getItemByName(@PathVariable(value = "name") String name) {
        return itemService.getItemByName(name);
    }

    /**
     * Recap: Items and Orders on ManyToMany relationship. Order is the parent.
     * if you want to remove some item, be sure that the specific item doesnt refer to
     * some order.
     * for more details check the table 'orders_items' at sqlite database.
     *
     * @return
     */
    @DeleteMapping("")
    public @ResponseBody
    String removeAllItems() {
        itemService.removeAll();
        return "All items removed.";
    }
}
