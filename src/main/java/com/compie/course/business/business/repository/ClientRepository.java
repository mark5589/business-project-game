package com.compie.course.business.business.repository;

import com.compie.course.business.business.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  This will be AUTO IMPLEMENTED by Spring into a Bean called clientRepository
 *  CRUD refers Create, Read, Update, Delete
 */
public interface ClientRepository extends JpaRepository<Client, Long> {
    
}
