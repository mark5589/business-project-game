package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Item;
import java.util.List;

public interface ItemService {

    Item saveItem(Item item);

    Item getItemByName(String name);

    List<Item> getAllItems();

    List<Item> getAllItemsFromList(List<String> list);

    void removeAll();

    void removeItemById(Long id);
}
