package com.compie.course.business.business.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * \
 * [*] Name of the class: Originally the name was Order, buy due the save key word in SQL 'Order by' I had to change that name to
 * Orders.
 * <p>
 * [*] SQL Relationship:: Each Order may have only one Client, but a Client may have more than one Order.
 */
@Entity
@Data
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private OrderType type;
    private Status status;

    /**
     * Loading the entities by EAGER approach,
     * doesnt require @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
     * The purpose of the above mentioned annotation:
     * As you can see, there is use in Client, Item Classes, each one is defined with
     * a relationship to the Order Class, ManyToOne and ManyToMany respectively.
     * without this annotation, the program falls on finding the right entity.
     * if there is a need to load the entity, when the fetch id EAGER, then all related
     * entities in the relationship would load too. there for if you want it loaded without
     * all related entities, then use FetchType.LAZY, with the @JsonIgnoreProperties(...)
     *
     * solution link ref: https://stackoverflow.com/questions/52656517
     */
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "client_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Client client;

    @ManyToMany(
            fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.PERSIST, CascadeType.MERGE
            })

    @JoinTable(name = "orders_items",
            joinColumns = {@JoinColumn(name = "orders_id")},
            inverseJoinColumns = {@JoinColumn(name = "item_id")})
    private List<Item> itemsList;

    @CreationTimestamp
    private LocalDateTime createdAt;

    public Order() {
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", type=" + type +
                ", status=" + status +
                ", createdAt=" + createdAt +
                '}';
    }
}
