package com.compie.course.business.business.exception;

import com.compie.course.business.business.entity.Item;

public class ItemExistException extends RuntimeException {
    public ItemExistException(Item item){
        super("The item: " + item.getName() + " (Id): "+ item.getId() + " Already Exist.");
    }
}
