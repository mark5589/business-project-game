package com.compie.course.business.business.exception;

public class OrderNotFoundException extends RuntimeException {
    public OrderNotFoundException(Long id){
        super("Could not find order " + id);
    }
}
