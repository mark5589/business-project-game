package com.compie.course.business.business.service;

import com.compie.course.business.business.entity.Client;
import com.compie.course.business.business.resource.ClientResourceAssembler;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class Convert Client, Order objects into ResponseEntity objects for the REST-ful representation of the
 * response.
 */

@Service
@AllArgsConstructor
public class ResponseConverterService {

    private final ClientResourceAssembler clientResourceAssembler;

    public ResponseEntity<EntityModel<Client>> getClientModelResponse(Client client){
        EntityModel<Client> clientEntityModel = clientResourceAssembler.toModel(client);
        return ResponseEntity.ok().body(clientEntityModel);
    }

    public ResponseEntity<List<EntityModel<Client>>> getClientListResponse(List<Client> clients){
        List<EntityModel<Client>> clientsList = clients.stream()
                .map(clientResourceAssembler::toModel)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(clientsList);
    }

    public ResponseEntity<EntityModel<Client>> getClientModelResponseOnPOST(Client client) throws URISyntaxException {
        EntityModel<Client> entityClient = clientResourceAssembler.toModel(client);
        URI uri = new URI(entityClient.getLink("self").get().getHref());
        return ResponseEntity.created(uri).body(entityClient);

    }

}
